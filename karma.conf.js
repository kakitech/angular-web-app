// Karma configuration
//http://karma-runner.github.io/0.13/config/configuration-file.html

module.exports = function(config) {
  config.set({

    basePath: '',

    frameworks: ['jasmine'],

    browsers: ['PhantomJS'],

		singleRun: true,

    // list of files / patterns to load in the browser
    files: [
      // Vendor js
  		"assets/js/lib/angular/angular.js",
  		"assets/js/lib/angular-ui-router/release/angular-ui-router.js",
  		"assets/js/lib/jquery/dist/jquery.js",
  		"assets/js/lib/bootstrap/dist/js/bootstrap.js",
  		"assets/js/lib/lodash/lodash.js",
  		"assets/js/lib/restangular/dist/restangular.js",

      // Angular Mocks
      "assets/js/lib/angular-mocks/angular-mocks.js",

  		// App js
      "app/**/*.js",

      // Test specs
      "tests/*.tests.js"
    ],    

  });
};