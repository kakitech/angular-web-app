/*
    Contact Route  
 */

(function () {

    'use strict';

    var modulePath = "app/modules/contacts/";

    angular.module('app.contacts')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
               .state('contact', {
                    url: '/contact',
                    templateUrl: modulePath + 'views/contact.html',
                    controller: 'contactCtrl'
                }); 
      }]);

})();
