/*
    Contact Controller  
 */
(function () {

    'use strict';

    var contactsImages = {
    		phoneimg: 'assets/img/address_book.png'
    	};

	angular.module('app.contacts', [])
	.controller('contactCtrl', ['$scope', '$http', function($scope) {

		$scope.message = 'Contact us! JK. This is just a demo.';  

		$scope.images = contactsImages;

	}]);

	
})();
