/*
    About Controller  
 */
(function () {

    'use strict';

	angular.module('app.about', [])
	.controller('aboutCtrl', ['$scope', '$http', function($scope) {

       $scope.message = 'Look! I am an about page.';

	}])
	.controller('tableCtrl', ['$scope', '$http', function($scope) {

		$scope.message = 'test';

		$scope.scotches = [
			{
				name: 'Macallan 12',
				price: 50
			},
			{
				name: 'Chivas Regal Royal Salute',
				price: 10000
			},
			{
				name: 'Glenfiddich 1937',
				price: 20000
			}
		];

	}]);

		
})();
