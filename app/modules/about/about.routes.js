/*
    About Route  
 */

(function () {

    'use strict';

    var modulePath = "app/modules/about/";

    angular.module('app.about')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
               .state('about', {
                    url: "/about",
                    views: {
                        // the main template of this url
                        '': { 
                                templateUrl: modulePath + 'views/about.html' ,
                                controller: 'aboutCtrl'
                            },

                        // the child views will be defined here (absolutely named)
                        'columnOne@about': { template: 'Look I am a column!' },

                        // for column two, we'll define a separate controller 
                        'columnTwo@about': { 
                            templateUrl: modulePath + 'views/table-data.html',
                            controller: 'tableCtrl'
                        }
                    }                    
                });
      }]);

})();
