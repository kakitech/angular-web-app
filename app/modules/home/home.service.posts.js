/*
    Posts Service  
 */
(function () {

    'use strict';

	angular.module('app.home')
	.factory('homeServicePosts', ['Restangular', '$q', function homeServicePosts(Restangular, $q) {
        return {
            getAllPosts: function() {
                var allPosts = Restangular.all('posts').getList().$object;
                
                return allPosts;
            },
            getAllPeople: function() {
                var allPeople = Restangular.all('people').getList().$object;
                
                return allPeople;
            }   

        };
    }]);

		
})();
