/*
    Home Controller  
 */
(function () {

    'use strict';

	angular.module('app.home', [])
	.controller('homeCtrl', [
			'$scope', 
			'configuration',
			'homeServicePosts',
			function(
					$scope, 
					configuration,
					homeServicePosts
					) {

       $scope.message =  'Environment: ' + configuration.environment;


	}])
	.controller('homeListCtrl', ['$scope', 'homeServicePosts', function($scope, homeServicePosts) {

       $scope.dogs = ['Bernese', 'Husky', 'Goldendoodle'];

       // Try call API /posts
       $scope.allPosts = homeServicePosts.getAllPosts();   

       $scope.allPeople = homeServicePosts.getAllPeople();

	}]);

		
})();
