/*
    Home Route  
 */

(function () {

    'use strict';

    var modulePath = "app/modules/home/";

    angular.module('app.home')
    .config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {

        $stateProvider
               .state('home', {
                    url: "/home",
                    templateUrl: modulePath + 'views/home.html',
                    controller: "homeCtrl"
                }) 
                // Nested inside home.html     
                .state('home.list', {
                    url: '/list',
                    templateUrl: modulePath + 'views/home-list.html',
                    controller: 'homeListCtrl'
                })
                // nested list with just some random string data
                .state('home.paragraph', {
                    url: '/paragraph',
                    template: 'I could sure use a drink right now.'
                });  
      }]);

})();
