/*
  <appnav></appnav> Directives
*/

(function () {

  'use strict';

  // Directive config
  var directiveConfig = {
      "tag" : "appnav",
      "templateUrl" : "app/directives/appnav/appnav.html"
  };


  // Bind to main App
  angular.module("app")
  .directive(directiveConfig.tag,['$templateRequest', '$compile', function($templateRequest, $compile){

      var linker =function(scope, element, attrs) {
    
          /*
             Directives root directory path
             Usage : <img ng-src="{{rootDirectory}}/images/avatar.png" alt="" />
          */
          scope.rootDirectory = "app/directives";

          // Compile template
          $templateRequest(directiveConfig.templateUrl).then(function(html){
            var template = angular.element(html);
            element.append(template);
            $compile(template)(scope);
         });
      };

      return {
        restrict: "E",
        link: linker,
        scope: {
            content:'='
            }
      };
  }]);


})();
