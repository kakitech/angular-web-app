(function () {

    'use strict';
    
    // Main App
    angular.module("app", [
		"ui.router",
        "restangular",        
        "app.config",
        "app.home",
        "app.about",
        "app.contacts",
        // add new modules here
    ])
    .config([
            "$stateProvider", 
            "$urlRouterProvider", 
            "RestangularProvider",
            "configuration", 
            function(
                    $stateProvider, 
                    $urlRouterProvider, 
                    RestangularProvider,
                    configuration,
                    // add new directives here
                    appnav
                    ) {

        // Default Route
        $urlRouterProvider.otherwise('/home');

        // Set Default API host name
        RestangularProvider.setBaseUrl(configuration.apiEndpoint);
        
    }]);

})();
