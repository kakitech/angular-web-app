
var controllerName = 'homeListCtrl';

describe(controllerName, function() {  

	// Inject mocked module
	beforeEach(module('app'));

	var $controller;

  beforeEach(inject(function(_$controller_){
    // The injector unwraps the underscores (_) from around the parameter names when matching
    $controller = _$controller_;
  }));

  describe('$scope.dogs', function() {
  	
  	it("must have 3 types of dogs", function(){
  		// access controller $scope
			var $scope = {};
      var controller = $controller(controllerName, { $scope: $scope });
      expect($scope.dogs.length).toEqual(3); 	
  	});

  });	


});