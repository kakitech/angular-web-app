/*
  Grunt Config
*/

module.exports = function(grunt) {
  
  // Load all grunt-* from package.json
  require("matchdep").filterDev("grunt-*").forEach(grunt.loadNpmTasks);
  
  // Time how long tasks take. Can help when optimizing build times
  require('time-grunt')(grunt);

  // Initialize grunt
  grunt.initConfig({
    /*
        NPM Packages
    */  
    pkg: grunt.file.readJSON('package.json'),

    /*
      Project Config
    */
    app: grunt.file.readJSON('config/project.json'),

    /*
        Environment / Constants
     */
    config: {
      dev: {
        options: {
          variables: '<%= app.ENV.DEV %>'
        }
      },
      prod: {
        options: {
          variables:  '<%= app.ENV.PROD %>'
        }
      }
    },

    replace: {
      dist: {
        options: {
          variables: {
            'environment': '<%= grunt.config.get("environment") %>',
            'apiEndpoint': '<%= grunt.config.get("apiEndpoint") %>'
          },
          force: true
        },
        files: [
          {expand: true, flatten: true, src: ['config/app.config.js'], dest: 'app/'}
        ]
      },

    },

    /*
      Inject IONIC library
    */
    'string-replace': {
      ionic: {
        files: {
          'ionic/index.html': 'index.html',
        },
        options: {
          replacements: [
            {
              pattern: /<!-- IONIC -->[\s\S]*?<!-- IONICEND -->/g,
              replacement: '<script src="assets/lib/ionic/js/ionic.bundle.js"></script><script src="cordova.js"></script>'
            }
          ]
        }
      },

      uncompressed_bower_clear: {
        files: {
          'index.html': 'index.html',
        },
        options: {
          replacements: [
            {
              pattern: /<!-- bower:js -->[\s\S]*?<!-- endbower -->/g,
              replacement: '<!-- bower:js --><!-- endbower -->'
            },
            {
              pattern: /<!-- bower:css -->[\s\S]*?<!-- endbower -->/g,
              replacement: '<!-- bower:css --><!-- endbower -->'
            },
            {
              pattern: /<!-- injector:css -->[\s\S]*?<!-- endinjector -->/g,
              replacement: '<!-- injector:css --><!-- endinjector -->'
            },            

          ]
        }
      }

    },        


    /*
      Compress CSS
     */
    cssmin: {
      vendor_css: {
        files: {
          'assets/css/app.vendor.min.css': require('wiredep')().css
        }
      },
      app_css: {
        files: {
          'assets/css/main.min.css': [ 'app/**/css/*.css' ]
        }
      }
    },    

    /*
      Compress Javascript
     */
    uglify: {
      options: {  
        compress: true  
              }, 
      vendor_js: {
        files: {
          'assets/js/app.vendor.min.js': require('wiredep')().js
        }
      },
      app_js: {
        files: {
          'assets/js/main.min.js':  ["app/*.js","app/**/*.js"]
        }
      }
    },
   

    /*
      Inject Bower Components
    */
    wiredep: {
      task: {
        src: [ 'index.html']
      }
    }, 

    /*
      Inject custom Javascript and Style sheets
    */
    injector: {
      options: {
        relative:true,
        template :'index.html',
        destFile: 'index.html'
      },
      files: ['assets/js/*.js', 'assets/css/*.css']

    },

    /*
      Compile SASS/SCSS to CSS
    */
    sass: {
        dist: {
            files: {
                'app/modules/common/css/compiled.css': 'app/**/scss/main.scss'
            }
        }
    },


    /*
      Concatenate files
     */
    concat: {
        app_js: {
          src: [ "app/*.js", "app/**/*.js" ],
          dest : 'assets/js/app.min.js'      
        },
        app_css: {
          src: [ "app/**/css/*.css", "assets/css/compiled.css" ],
          dest : 'assets/css/app.min.css'      
        }        
    },


    /*      
      Watches for any changes in CSS and Javascript
     */
    watch: {
      options: {
        spawn: false, // Important, don't remove this!
         event: ['changed', 'added', 'deleted']
      },
      css: {
        files: ['app/**/css/*.css', 'app/**/scss/*.scss'],
        tasks: ['sass', 'cssmin:app_css', 'injector']
      },
      js: {
        files: [  
                  'Gruntfile.js',                  
                  'app/app.js',
                  'app/**/*.js',
                ],
        tasks: ['jshint','uglify:app_js']
      },
      html: {
        files: ['index.html', 'app/**/*.html'],
        tasks: ['uglify', 'cssmin', 'copy:img']
      }      
    },
    /*
      Validates javascript for any error
     */
    jshint: {
        all : {
          src : [
                  'app/app.js',
                  'app/**/*.js'
                ],
          options : {
            jshintrc : '.jshintrc'
          }
        },
    },

    /*
        Clear folder files
     */
    clean: {
      config:['app/app.config.js'],
      css: ['assets/css/*.css'],
      img: ['assets/img/*.{png,gif,jpg,ico}'],
      js:  ['assets/js/*.js', '!assets/js/lib/ionic/*'],
      ionic: ['ionic/'],
      fonts: ['assets/fonts/'],
      cssmap: ['app/**/*.map']
    },

    /*
      Copy files
     */    
    copy: {
        options: {
          //processContentExclude: [ '**/*.{png,gif,jpg,ico,psd}' ]
        }, 
        // Copy font files to assets/fonts
        fonts : {
          files: [
            {
              expand: true, 
              src: '<%= app.FONTS %>', 
              dest: 'assets/fonts/',
              flatten: true,
              filter: 'isFile'
            },
          ]
        },        
        // Copy modules image files to assets/img     
        img : {
          files: [
            {
              expand: true, 
              src: ['app/**/img/*'], 
              dest: 'assets/img/',
              flatten: true,
              filter: 'isFile'
            },
          ]
        },
        // copy prod files for IONIC convertion
        ionic : {
          files: [
            {
              expand: true, 
              src: ['app/**/*','assets/**/*'], 
              dest: 'ionic/',
              flatten: false,
              filter: 'isFile'
            },
          ]
        }        
    },

    /*
      Serve application and automatically inject updated files
     */
    browserSync: {
      files: [
            "assets/css/*.css",
            "assets/js/*.js",
            "assets/img/*.{png,jpg,jpeg,gif,webp,svg}"
            ],     
      options: {
        watchTask: true,
        injectChanges: true,
        server: "./"
      },

    },
    /*
      Mock backend API
     */
    json_server: {
      dev: {
        options: {
            port: '<%= app.MOCK_API_PORT %>',
            db: '<%= app.MOCK_DATABASE %>'
        }
      }
    },         
    /*
      Run parallel tasks
     */
    concurrent: {
        dev: {
            tasks: [              
                'jshint',
                'copy:img',
                'copy:fonts',
                'wiredep',
                'concat'              
            ],
            options: {
                limit:10,
                logConcurrentOutput: true
            }
        },
        prod: {
            tasks: [
                'jshint',
                'copy:img',
                'copy:fonts',
                'uglify', 
                'cssmin'
            ],
            options: {
                limit:10,
                logConcurrentOutput: true
            }
        }
    },

    // Test settings
    karma: {
      // Unit Test
      unit: {
        configFile: 'karma.conf.js',
        reporters: ['mocha'],
        mochaReporter: {
          colors: {
            success: 'blue',
            info: 'bgGreen',
            warning: 'cyan',
            error: 'bgRed'
          }
        },
        plugins: [
          'karma-jasmine',
          'karma-mocha-reporter',
          'karma-phantomjs-launcher'
        ]         
      },
      // Code Coverage
      coverage: {
        configFile: 'karma.conf.js',

        // coverage reporter generates the coverage 
        reporters: ['progress', 'coverage'],

        // source files, that you wanna generate coverage for 
        preprocessors: { 'app/**/*.js': ['coverage'] },

        // Folder and Type
        coverageReporter: {
          type : 'html',
          dir : 'tests/coverage/'
        }
      }
    },    


    // Add new config here..    
  });  

  /*
    DEVELOPMENT Tasks
  */
  grunt.registerTask('dev', function(target, option) {
    
    // $> grunt dev:test
    if (target === 'test') {
      
      // $> grunt dev:test:unit  
      if (option === 'unit') {
        return grunt.task.run([
          //run unit test
          'jshint',
          'karma:unit'

        ]);
      }
      // $> grunt dev:test:coverage  
      else if (option === 'coverage') {
        return grunt.task.run([
          // run code coverage
          'jshint',
          'karma:coverage'

        ]);
      }

      else {
        return grunt.task.run([
          // Run unit and coverage

        ]);
      }      

    }


    // $> grunt dev
    else grunt.task.run([
      'clean',
      'config:dev', 
      'replace:dist', 
      'sass',
      'concurrent:dev',
      'injector',
      'browserSync', 
      'watch'
    ]);

  });

  /*
    PRODUCTION Tasks
  */
  grunt.registerTask('prod', function(target, option) {
    
    // $> grunt prod:test
    if (target === 'prod') {
      
      // $> grunt dev:test:unit  
      if (option === 'unit') {
        return grunt.task.run([
          //run unit test

        ]);
      }
      // $> grunt prod:test:coverage  
      else if (option === 'coverage') {
        return grunt.task.run([
          // run code coverage

        ]);
      }

      else {
        return grunt.task.run([
          // Run unit and coverage

        ]);
      }      

    }


    // $> grunt prod
    else grunt.task.run([
      'clean',
      'string-replace:uncompressed_bower_clear',      
      'config:prod', 
      'replace:dist', 
      'sass',
      'concurrent:prod', 
      'injector',
      'browserSync', 
      'watch'
    ]);
      
  });

  
  // $> grunt api
  grunt.registerTask('api', [ 'json_server' ]);

  // $> grunt ionic
  grunt.registerTask('ionic', [ 'clean:ionic', 'config:prod', 'replace:dist', 'string-replace:ionic', 'concurrent:prod', 'copy:ionic' ]);
};