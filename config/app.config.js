/*
	Managed App constants
*/
(function () {
  	
  	'use strict';
	
	angular.module('app.config', [])
	.constant('configuration', {
	    environment: '@@environment',
	    apiEndpoint: '@@apiEndpoint'
	});

})();	