/*
		This requires :
		npm i faker lodash --save
 */
var faker = require("faker");
var _ = require("lodash");

exports.run = function() {
    var data = {}, i = 0;

    data.posts = [];
    while(i < 100) {
        data.posts.push({ id: i, body: 'foo' + i });
        i++;
    }

    // Using Faker
    data.people	= _.times(100, function(n) {
    	return {
    				id: n,
    				name: faker.name.findName(),
    				avatar: faker.internet.avatar()
    				};
    });
    
    // return data
    return data;
};