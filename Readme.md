## Structured AngularJS Web Application

### Features
- Centralised application and build config [config/project.json](config/project.json)
- Modules are seperated in a per component/module basis
- Each modules has their own controller, views, css, images, fonts and routes
- Dev environment uses uncompressed javascript and css for faster debuging
- Automatically inject bower components javascript and CSS to index.html
- Automatically inject minified and non-minified javascript and CSS via injector
- Validates javascript changes before compressing
- Watches any changes on JS, CSS and HTML
- Stand alone server
- View real-time changes on different devices ( Desktop, Tablet, SmartPhone )
- Sync all changes to all current devices connected
- Automatically opens a browser for you after executing "$> grunt {env}"
- Can mock backend API to prevent dependency on Backend Developers
- Easily convert Web Application to Hybrid Application via IONIC framework
- SASS / SCSS and Normal CSS support for all modules and directives
- Automatically concatenate both normal css and compiled SASS/SCSS for dev and prod(compressed)

### On WIP
- Module Generator
- Directive Generator


### AngularJS Modules
- angular-ui-router
- bootstrap
- faker
- restangular 
- browser-sync
- json-server
- ionic framework

### Installations needed
1.) Following needs to be installed globally on your machine
 - [Git](https://git-scm.com/downloads)
 > git --version

 - [NodeJs](https://nodejs.org/en/download/)
 - We just need npm packager that depends on NodeJs
 > npm -v

 - [Grunt-cli](http://gruntjs.com/getting-started)
 > npm install -g grunt-cli

 - [Bower](http://bower.io/#install-bower)
 > npm install -g bower

 - [Ruby](http://rubyinstaller.org/downloads/)
 > ruby -v

 - [SASS](http://sass-lang.com/install)
 > gem install sass


2.) Clone this [repository](https://gitlab.com/kakitech/angular-web-app.git) and run command to initialized your libraries and build tools
 
> $> git clone https://gitlab.com/kakitech/angular-web-app.git mywebsite
 
> $> cd mywebsite
 
> $> npm install && bower install && grunt dev

### How to run your project

- Generate build files

> grunt dev

or

> grunt prod

### How to run Unit Test
- Write your jasmine specs in "tests/" folder
- Run Unit Test

> grunt dev:test:unit

or

> grunt prod:test:unit

### How to run code coverage
- Coverage report will be generated in "tests/coverage"
- Generate code coverage

> grunt dev:test:coverage

or

> grunt prod:test:coverage


### How to access API

- Run grunt task

> grunt api

- Access API document page

> http://localhost:5009


## How to convert Web App to IONIC Hybrid App

- Run grunt task

> grunt ionic

- Then copy the files inside "ionic" folder to IONIC blank template "www" folder
- From there do 

> $> ionic serve 

- to see the web app works on hybrid environment


## How to Install bower components
- <package_name> are available in [bower.io](http://bower.io/search/)

> bower install <package_name> --save


## Side NOTE
 By default, all user css,images,js, fonts are ignored, if you plan to track your changes, you need to re-initialised your 
 project repository and modify ".gitignore" to allow tracking of css,images,js,fonts
 
 > $> rm -rf .git
 
 > $> git init
 
 See [.gitignore](.gitignore) to know which part to remove to be able to track your files